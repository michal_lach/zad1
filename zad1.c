/*Napisać program, który wczyta plik P0001 attr.rec z mojej strony i utworzy 3 tablice
dynamiczne floatów, do których zaczyta wartości X, Y i RHO. Zostanie policzona i wyświetlona średnia, mediana i odchylenie standardowe dla X, Y i RHO. 
Wyniki statystyk zostaną dopisane do pierwotnego pliku. W modułach mają znaleźć się tylko funkcje/procedury dla statystyk.*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "fun.h"

// float *plikdotab(FILE *fp, int w, int k);

//Tutaj zaczyna sie main

int main()
{


    FILE *fp;   //kod wczytujący tablice z pliku do trzech dynamicznych tablic floatów
    int x = 50;
    if ((fp = fopen("plik.txt", "r")) == NULL)
    {
        printf("Error");
        exit(1);
    }
    char p[10] = "";
    char q[10] = "";
    char r[10] = "";
    char s[10] = "";
    fscanf(fp, "%s\t%s\t\t%s\t\t%s\n", p, q, r, s); //zczytanie nagłówka
    float *tab0;
    float *tab1;
    float *tab2;
    int i = 0;
    tab0 = (float*)malloc(x*sizeof(float));    //zaalokowanie tablic dynamicznych
    tab1 = (float*)malloc(x*sizeof(float));
    tab2 = (float*)malloc(x*sizeof(float));
    float t = 0.0;
    float u = 0.0;
    float v = 0.0;
    for (i = 0; i < x; i++) //pętla czytająca każdy wiersz
    {
        fscanf(fp, "%s\t%f\t\t%f\t%f\n", s, &t, &u, &v);
        tab0[i] = t;
        tab1[i] = u;
        tab2[i] = v;
    }
    //////////////////////////
    // int x = 50;
    
    int k = 0;
    // float *tab0;
    // tab0 = plikdotab("plik.txt", x, k);    //funkcja zczytania danej kolumny tablicy z pliku
    //druktab(tab0, x);   //funkcja dla pokazania tablicy
    float srednia0 = 0.0;
    srednia0 = average(tab0, x, k);    //funkcja dla sredniej kolumny
    float mediana0 = 0.0;
    mediana0 = mediantab(tab0, x, k); //funkcja dla mediany
    float odchylenie0 = 0.0;
    odchylenie0 = odchylenietab(tab0, x, k, srednia0);    //funkcja dla odchylenia

    k = 1;
    // float *tab1;
    // tab1 = plikdotab("plik.txt", x, k);
    float srednia1 = 0.0;
    srednia1 = average(tab1, x, k);    //funkcja dla sredniej kolumny
    float mediana1 = 0.0;
    mediana1 = mediantab(tab1, x, k); //funkcja dla mediany
    float odchylenie1 = 0.0;
    odchylenie1 = odchylenietab(tab1, x, k, srednia1);

    k = 2;
    // float *tab2;
    // tab2 = plikdotab("plik.txt", x, k);
    float srednia2 = 0.0;
    srednia2 = average(tab2, x, k);    //funkcja dla sredniej kolumny
    float mediana2 = 0.0;
    mediana2 = mediantab(tab2, x, k); //funkcja dla mediany
    float odchylenie2 = 0.0;
    odchylenie2 = odchylenietab(tab2, x, k, srednia2);

    dopisz("plik.txt", srednia0, mediana0, odchylenie0, srednia1, mediana1, odchylenie1, srednia2, mediana2, odchylenie2); // procedura dopisuje statystyki do pliku

    return 0;
}
// float *plikdotab(FILE *fp, int w, int k)    //procedura zczytania danej kolumny tablicy z pliku (plik, ilosc wierszy, kolumna)
// {
//     if ((fp = fopen(fp, "r")) == NULL)
//     {
//         printf("Error");
//         exit(1);
//     }
//     char p[4] = "";
//     char q[4] = "";
//     char r[4] = "";
//     char s[4] = "";
//     fscanf(fp, "%s\t%s\t\t%s\t\t%s\n", p, q, r, s); //zczytanie nagłówka
//     float *tab;
//     int i = 0;
//     tab = (float**)malloc(w*sizeof(float*));    //tablica dynamiczna
//     float t = 0.0;
//     float u = 0.0;
//     float v = 0.0;
//     for (i = 0; i < w; i++) //pętla czytająca każdy wiersz
//     {
//         fscanf(fp, "%s\t%f\t\t%f\t%f\n", s, &t, &u, &v);
//         if (k == 0)
//         {
//             tab[i] = t;
//         }
//         else if (k == 1)
//         {
//             tab[i] = u;
//         }
//         else if (k == 2)
//         {
//             tab[i] = v;
//         }
//         else
//         {
//             printf("Blad nazwy kolumny!");
//             return;
//         }
//     }
//     free(tab);
//     fclose(fp);
//     return tab;
// }

//wszystko smiga








