#include <stdio.h>
#include "fun.h"
float mediantab(float *tab, int x, int k)  //funkcja liczenia mediany
{
    float temp = 0.0;   //sortowanie tablicy 110 - 122
    for (int i = 0; i < x; i++)
    {     
        for (int j = i+1; j < x; j++) 
        {     
           if(tab[i] > tab[j]) 
           {    
               temp = tab[i];    
               tab[i] = tab[j];    
               tab[j] = temp;    
           }     
        }     
    }
    float mediana = 0.0;
    int y = x / 2;  //indeks polowy tablicy
    mediana = tab[y];   //wartosc tego indeksu

    char p[4] = "";
    if (k == 0)
    {
        strcpy(p, "X");
    }
    else if (k == 1)
    {
        strcpy(p, "Y");
    }
    else if (k == 2)
    {
        strcpy(p, "RHO");
    }

    printf("Mediana kolumny %s wynosi: %f\n", p, mediana);
    return mediana;
}