CC=gcc
CFLAGS=-Wall 
LIBS=-lm 
zad1: zad1.o average.o mediantab.o odchylenietab.o druktab.o dopisz.o
	$(CC) $(CFLAGS) -o zad1 zad1.o average.o mediantab.o odchylenietab.o druktab.o dopisz.o $(LIBS)
zad1.o : zad1.c fun.h 
	$(CC) $(CFLAGS) -c zad1.c
dopisz.o : dopisz.c fun.h
	$(CC) $(CFLAGS) -c dopisz.c
druktab.o : druktab.c fun.h
	$(CC) $(CFLAGS) -c druktab.c
odchylenietab.o : odchylenietab.c fun.h
	$(CC) $(CFLAGS) -c odchylenietab.c
mediantab.o : mediantab.c fun.h
	$(CC) $(CFLAGS) -c mediantab.c
average.o : average.c fun.h
	$(CC) $(CFLAGS) -c average.c
