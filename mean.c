#include "fun.h"

float mean(float x[], int elem)
{
    int i=0;
    float res = 0.0;

    for(i=0; i<elem; i++){
        res+=x[i];
    }
    res /= elem;
    return res;

}